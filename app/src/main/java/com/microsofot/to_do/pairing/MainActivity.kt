package com.microsofot.to_do.pairing

import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.moshi.Json
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

class MainActivity : AppCompatActivity() {

    private val boardingCardsJson = "[{\"id\":\"vswqtf\",\"origin\":\"Frankfurt\",\"destination\":\"Seattle\",\"means_transportation\":\"plane\",\"seat\":\"12D\",\"image_url\":\"https://c1.staticflickr.com/8/7291/26885642423_2678c9824a_b.jpg\",\"color\":\"#f47461\"},{\"id\":\"yptgxx\",\"origin\":\"Berlin\",\"destination\":\"Tegel\",\"means_transportation\":\"bus\",\"seat\":null,\"image_url\":\"https://upload.wikimedia.org/wikipedia/commons/a/af/Berlin-Tegel_from_the_air.jpg\",\"color\":\"#ffd59b\"},{\"id\":\"izsugq\",\"origin\":\"Redmond\",\"destination\":\"Microsoft HQ\",\"means_transportation\":\"bus\",\"seat\":null,\"image_url\":\"https://upload.wikimedia.org/wikipedia/commons/1/1a/Microsoft_building_17_front_door.jpg\",\"color\":\"#b81b34\"},{\"id\":\"kjting\",\"origin\":\"Seattle\",\"destination\":\"Redmond\",\"means_transportation\":\"train\",\"seat\":\"Coach 5 - 10B\",\"image_url\":\"https://c1.staticflickr.com/7/6220/6316634795_ecd798718c_b.jpg\",\"color\":\"#db4551\"},{\"id\":\"kqhubo\",\"origin\":\"Potsdam\",\"destination\":\"Berlin\",\"means_transportation\":\"train\",\"seat\":null,\"image_url\":\"https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/16-Berlin-Hbf-S.jpg/1280px-16-Berlin-Hbf-S.jpg\",\"color\":\"#ffffe0\"},{\"id\":\"khozlx\",\"origin\":\"Tegel\",\"destination\":\"Frankfurt\",\"means_transportation\":\"plane\",\"seat\":\"7A\",\"image_url\":\"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Aerial_View_of_Frankfurt_Airport_1.jpg/1024px-Aerial_View_of_Frankfurt_Airport_1.jpg\",\"color\":\"#ffa474\"}]"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
        val listType = Types.newParameterizedType(List::class.java, BoardingCard::class.java)
        val moshiAdapter: JsonAdapter<List<BoardingCard>> = moshi.adapter(listType)

        val adapter = BoardingCardsAdapter()
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        adapter.setDataSet(moshiAdapter.fromJson(boardingCardsJson)!!)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.sort -> {
                // todo
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    class BoardingCardsAdapter : RecyclerView.Adapter<BoardingCardViewHolder>() {

        lateinit var boardingCards: List<BoardingCard>

        fun setDataSet(boardingCards: List<BoardingCard>) {
            this.boardingCards = boardingCards
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoardingCardViewHolder {
            return BoardingCardViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.boarding_card_item, parent, false))
        }

        override fun getItemCount(): Int {
            return boardingCards.size
        }

        override fun onBindViewHolder(holder: BoardingCardViewHolder, position: Int) {
            holder.setData(boardingCards[position].id)
        }

    }

    class BoardingCardViewHolder constructor(val view: View) : RecyclerView.ViewHolder(view) {

        fun setData(id: String) {
            view.findViewById<TextView>(R.id.textview).text = id
        }

    }

    class BoardingCard(val id: String,
                       val origin: String,
                       val destination: String,
                       @Json(name = "means_transportation") val meansTransportation: String,
                       val seat: String?,
                       @Json(name = "image_url") val imageUrl: String,
                       val color: String)

}
